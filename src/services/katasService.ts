import { AxiosRequestConfig } from "axios";
import axios from "../utils/config/axios.config";

/**
 * Get the list of katas method
 * @param token 
 * @param limit 
 * @param page 
 * @returns 
 */
export const getAllKatas = (token: string, limit?:number, page?: number) => {

    // http://localhost:8000/api/katas?limit=2&page=2
    // Add headers with JWT in x-access-token, it could be retrieved using session storage but params allosws a better control of the variables

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            limit,
            page
        }
    }

    return axios.get('/katas', options)
        
}
/**
 * Get kata details method
 * @param token 
 * @param id 
 * @returns 
 */
export const getKataById = (token: string, id: string) => {
    // http://localhost:8000/api/katas?id
    // Add headers with JWT in x-access-token 

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            id
        }
    }
    return axios.get('/katas', options)
}

export const createKata = (token: string, name: string, description: string, level: any, stars: number, intents: number, participants: any[], solution: string) => {
    // Declare body to post
    let body = {
        name,
        description,
        level,
        stars,
        intents,
        participants,
        solution
    }

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        data: body
    }
    return axios.post('/katas', options)
}