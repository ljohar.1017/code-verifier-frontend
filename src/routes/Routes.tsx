


//React Router DOM Imports
import{ BrowserRouter as Router, Routes, Route, Navigate, Link } from 'react-router-dom'
import { HomePage } from '../pages/HomePage'
import { KatasDetailsPage } from '../pages/KatasDetailsPage'
import { KatasPage } from '../pages/KatasPage'
import { LoginPage } from '../pages/LoginPage'
import { RegisterPage } from '../pages/RegisterPage'


export const AppRoutes = () => {
    return(
        <Routes>
        {/* Routes definition */}
        <Route path='/' element={<HomePage/>}></Route>
        <Route path='/login' element={<LoginPage/>}></Route>
        <Route path='/register' element={<RegisterPage/>}></Route>
        <Route path='/katas' element={<KatasPage/>}></Route>
        <Route path='/katas/:id' element={<KatasDetailsPage/>}></Route>
        {/* Redirect when page not found */}
        <Route 
          path='*' 
          element={<Navigate to='/' replace/>}> {/*or create a 404 page*/}
        </Route>
      </Routes>
    )
}