import Typography  from "@mui/material/Typography";
import Link from '@mui/material/Link'

// TODO create an interface with the props
export const Copyright = (props: any) => {
    return(
        <Typography variant="body2" color="text.secondary" align="center" { ...props }>
            { 'CopyRigh © '}
            <Link color="inherit" href="https://gitlab.com/ljohar.1017">
                {/* TODO text and repo link trough props */}
                Joha's Repo
            </Link>
            {' '}
            { new Date().getFullYear()}
        </Typography>
    )
}