import { AxiosResponse } from "axios";
import React, { useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";


import { useSessionStorage } from "../hooks/useSessionStorage";
import { getAllKatas } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";

export const KatasPage = () => {

    // token
    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    // State of component
    const [katas, setKatas] = useState([]) // Initial Katas is empty
    const [totalPages, setTotalPages] = useState(1) // Initial default value
    const [currentPage, setCurrentPage] = useState(1) // Initial default value

    // TODO export to katas component
    // TODO define a type for katas
    // TODO add another case to if statements when JWT is not valid
    useEffect(() => {
        if(!loggedIn){
            return navigate('/login')
        }else{
            getAllKatas(loggedIn, 2, 1 ).then((response: AxiosResponse) => {
                if(response.status === 200 && response.data.katas && response.data.totalPages && response.data.currentPage){
                    console.table(response.data);

                    let { katas, totalPages, currentPage} = response.data
                    // setKatas(response.data.katas)
                    setKatas(katas);
                    setTotalPages(totalPages);
                    setCurrentPage(currentPage);
                }else{
                    throw new Error(`Error obtaining katas: ${response.data} `)
                }

            }).catch((error) => console.error(`[Get All Katas Error] ${error}`))
        }
    }, [loggedIn])

    /**
     * Method to navigate to Kata details
     * @param id of Kata yo navigate to
     */
    const navigateToKataDetails = (id: number) => {
        navigate(`/katas/${id}`);
    }

    return (
        <div>
            <h1>
                Katas Page
            </h1>

            { katas.length > 0 ?
                    <div>
                        
                        {/* TODO export to isolated component Should be a component and iterate over a list (initial useEffect to load katas, the iterate with map ), this is just an example */}
                        {/* TODO change color regarding the kata level */}
                        { katas.map((kata: Kata) => 
                            (
                                <div key={kata._id}>
                                    <h3 onClick={() => navigateToKataDetails(kata._id)}>{kata.name}</h3>
                                    <h4>{kata.description}</h4>
                                    <h5>Creator: {kata.creator} </h5>
                                    <p>Rating: {kata.stars}/5</p>
               
                                </div>

                            )
                        )}

                                
                    </div>
                :

                    <div>
                        {/* Define a variable to see if data is loading */}
                        <h5>
                            No katas found
                        </h5>
                    </div>

            }
        </div>
    )
}