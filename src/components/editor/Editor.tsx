import React from "react";

import Highlight, { defaultProps } from "prism-react-renderer";

interface EditorProps {
    language?: any,
    children?: any,
    solution?: any
}
const exampleCode = `
import React, { useState } from "react";

function Example() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}
`.trim();



export const Editor = ({ language, children, solution }: EditorProps ) => {
    return (
        <Highlight {...defaultProps} code={exampleCode} language='typescript' >
            {({ className, style, tokens, getLineProps, getTokenProps }) => (
                <pre className={className} style={style}>
                    {tokens.map((line, i) => (
                    <div {...getLineProps({ line, key: i })}>
                        {line.map((token, key) => (
                        <span {...getTokenProps({ token, key })} />
                        ))}
                    </div>
                    ))}
                </pre>
            )}
            
        </Highlight>
    );

}