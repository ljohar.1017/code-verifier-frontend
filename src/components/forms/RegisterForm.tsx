import React from "react";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { AxiosResponse } from "axios";

import { register } from "../../services/authServices";

// Define Schema of validation with Yup
const registerSchema = Yup.object().shape({
    name: Yup.string()
        .min(6, 'Username must have at least 6 characters long')
        .max(12, 'Username must be between 6 and 12 characers long')
        .required('Username is required'),
    email: Yup.string()
        .email('Invalid Email format')
        .required('Email is required'),
    password: Yup.string() // TODO implements regexr
        .min(8, 'Password too short')
        .required('Password is required'),
    confirm: Yup.string().when("password", {
        is: (value: string) => (value && value.length>0 ? true : false),
        then: Yup.string().oneOf(
            [Yup.ref("password")], 'Passwords must match'
        )
        })
        .required('You must confirm yor password'),
    age: Yup.number()
        .min(10, 'You must be over 10 years old')
        .required('Age is required') // TODO implements checkbox for this field
});

// Register Component
const ResgisterForm = () => {
    // Initial credentials (Formik)
    const initialValues = {
        name: '',
        email: '',
        password: '',
        confirm: '',
        age: 18
    }

    
    return(
        <div>
            <h4>
                Register Form
            </h4>
            {/* Formik wrapper */}
            <Formik
                initialValues = { initialValues }
                validationSchema = { registerSchema }
                onSubmit = { async(values) => { // TODO Create aysnc method for onSubmit outside the code block
                    // await new Promise((response) => setTimeout(response, 2000));
                    register(values.name, values.email, values.password, values.age).then((response: AxiosResponse) => {
                        if(response.status === 200){ // TODO change in the backend the status to 201 (creation)
                            console.log('User registered correctly')
                            console.table(response.data)
                            alert('User registered correctly')
                            }else{
                                throw new Error('Error in registry')
                            }             
                    }).catch((error) => console.error(`[REGISTER ERROR] Something went wrong: ${error}`))
                }}
            >

                {
                ({values, touched, errors, isSubmitting, handleChange, handleBlur }) =>
                (
                    <Form>
                        {/* Name field */}
                        <label htmlFor='name'>Name</label>
                        <Field id='name' type='text' name='name' placeholder='Your name'/> 
                        {/* Name Errors */}
                        {
                            errors.name && touched.name && (
                                <ErrorMessage name="name" component='div'></ErrorMessage>
                            )
                        }

                        {/* Email field */}
                        <label htmlFor='email' >Email</label>
                        <Field id='email' type='email' name='email' placeholder='example@email.com' />
                        {/* Email Errors */}
                        {
                            errors.email && touched.email && (
                                <ErrorMessage name="email" component='div'></ErrorMessage>
                            )
                        }

                        {/* Password field */}
                        <label htmlFor='password' >Password</label>
                        <Field id='password' type='password' name='password' placeholder='Password' />
                        {/* Password Errors */}
                        {
                            errors.password && touched.password && (
                                <ErrorMessage name='password' component='div'></ErrorMessage>
                            )
                        }

                        {/* Confirm Password field */}
                        <label htmlFor='confirm' >Confirm password</label>
                        <Field id='confirm' type='password' name='confirm' placeholder='Confirm your password' />
                        {/* Password Errors */}
                        {
                            errors.confirm && touched.confirm && (
                                <ErrorMessage name='confirm' component='div'></ErrorMessage>
                            )
                        }

                        {/* Age field */}
                        <label htmlFor='age' >Age</label>
                        <Field id='age' type='number' name='age' />
                        {/* Password Errors */}
                        {
                            errors.age && touched.age && (
                                <ErrorMessage name='age' component='div'></ErrorMessage>
                            )
                        }

                        {/* Submit form */}
                        <button type='submit'>Register</button>

                        {/* Message if the form is submitting */}
                        {
                            isSubmitting ? (<p>Sending data to registry</p>) 
                            : null
                        }

                    </Form>
                )
            }
            </Formik>

            
        </div>
    )
}

export default ResgisterForm