import { AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";

// React Router DOM imports
import { useNavigate, useParams } from "react-router-dom";
import { Editor } from "../components/editor/Editor";
import { useSessionStorage } from "../hooks/useSessionStorage";
import { getKataById } from "../services/katasService";
import { Kata } from "../utils/types/Kata.type";

export const KatasDetailsPage = () => {

    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    // Find id from params
    let { id } = useParams();
    // It's neccesary add types to useState in order to use setKata
    const [kata, setKata] = useState<Kata | undefined>(undefined);
    
    const [showSolution,setShowSolution] = useState(false)
    
    
    // TODO EXPORT HOOK
    useEffect(() => {
        if(!loggedIn){
            return navigate('/login')
        }else{
            if(id){
                getKataById(loggedIn, id). then((response: AxiosResponse) => {
                    if(response.status === 200 && response.data){
                        // Since API is returning version, properties were called individually insted of use spread operator
                        // TODO define how to share the kata solution
                        // TODO Apply kata type to setKata
                        let kataData = {
                            _id: response.data._id,
                            name: response.data.name,
                            description: response.data.description,
                            stars: response.data.stars,
                            level: response.data.level,
                            intents: response.data.intents,
                            creator: response.data.creator,
                            solution: response.data.solution,
                            participants: response.data.participants

                        }
                        // TODO  boolean to stablish if data loaded
                        // setKata(kataData)
                        setKata(kataData);
                        console.table(kataData);
                    }

                }).catch((error) => console.error(`[Get kata by id ERROR]: ${error}`))
            }else{
                return navigate('/katas');
            }
            
        }
    }, [loggedIn]);

    return (
        <div>
            <h1>
                Katas Details Page: { id }
            </h1>
            { kata ?
                <div className="kata-data">
                    <h2>{kata?.description}</h2>
                    {/* TODO Implement stars */}
                    <h3>Rating: {kata.stars}/5</h3>
                    <button onClick={() => setShowSolution(!showSolution)}>
                        {showSolution ? 'Show solution' : 'Hide solution'}
                    </button>
            
                    { showSolution ? null : <Editor>{kata.solution}</Editor> }
                </div>
            :
                <div>
                    <h2>
                        {/* TODO  */}
                        Loading data...
                    </h2>
                </div>
            }
            <button onClick={() => setShowSolution(!showSolution)}>
                {showSolution ? 'Show solution' : 'Hide solution'}
            </button>
            
            { showSolution ? null : <Editor solution={kata?.solution}></Editor> }
            {/* <Editor></Editor> */}
        </div>
    )
}