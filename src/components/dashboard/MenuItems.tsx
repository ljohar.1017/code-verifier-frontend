import React from "react";


// Material List Componentes
import ListItemButton  from "@mui/material/ListItemButton";
import ListItemIcon  from "@mui/material/ListItemIcon";
import ListItemText  from "@mui/material/ListItemText";

// Material Icon Components
import PeopleIcon from '@mui/icons-material/People';
import DashboardIcon from '@mui/icons-material/Dashboard';
import LeaderboardIcon from '@mui/icons-material/Leaderboard';

export const MenuItems = (
    <React.Fragment>
        {/* Dashboard to Katas Button */}
        <ListItemButton>
            <ListItemIcon>
                <DashboardIcon/>
            </ListItemIcon>
            <ListItemText primary="Katas"/>
        </ListItemButton>

        {/* Users */}
        <ListItemButton>
            <ListItemIcon>
                <PeopleIcon/>
            </ListItemIcon>
            <ListItemText primary="Users"/>
        </ListItemButton>

        {/* Ranking */}
        <ListItemButton>
            <ListItemIcon>
                <LeaderboardIcon/>
            </ListItemIcon>
            <ListItemText primary="Ranking"/>
        </ListItemButton>

    </React.Fragment>
)