import axios from "../utils/config/axios.config"; // instead of fetch

/**
 * Login method
 * @param email Email to login user
 * @param password Password to login user
 * @returns 
 */

export const login = (email: string, password: string) => {
    // Declare Body to Post
    let body = {
        email: email,
        password: password
    }

    // Send POST request to login endpoint
    // http://localhost:8000/api
    return axios.post('auth/login', body)
}

/**
 * Register method
 * @param name 
 * @param email 
 * @param password 
 * @param age 
 * @returns 
 */
export const register = (name: string, email: string, password: string, age: number) => {
    // Declare Body to Post
    let body = {
        name: name,
        email: email,
        password: password,
        age: age
    }

    // Send POST request to register endpoint
    // http://localhost:8000/api
    return axios.post('auth/register', body)
}