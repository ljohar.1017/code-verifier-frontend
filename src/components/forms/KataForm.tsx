import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { AxiosResponse } from "axios"

import { createKata } from "../../services/katasService";
import { useSessionStorage } from "../../hooks/useSessionStorage";

// Define Schema of validation with Yup
const kataSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    descritpion: Yup.string().required('Description field is required'),
    level: Yup.number().required('Level is required'),
    stars: Yup.number(),
    intents: Yup.number(),
    participants: Yup.array(),
    solution: Yup.string()
        .min(1,"You can't leave this blank" )
        .required('Solution is required')
})

// Kata component
const KataForm = () => {

    // token
    let loggedIn = useSessionStorage('sessionJWTToken');

    // Initial credentials (Formik)
    const initialValues = {
        name: '',
        description: '',
        level: 'Basic',
        stars: 0,
        intents: 0,
        participants: [],
        solution: '',
    }
    return(
        <div>
            <h4>
                Katas Form
            </h4>
            {/* <Formik
                initialValues = { initialValues }
                validationSchema = { kataSchema }
                onSubmit = { async(values) => {
                    createKata( values.name, values.description, values.level, values.stars, values.intents, values.participants, values.solution)
                }}
            >

            </Formik> */}
        </div>
    )
}