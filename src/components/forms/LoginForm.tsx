import React from "react";
import { useNavigate } from "react-router-dom";


import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { login } from "../../services/authServices";
import { AxiosResponse } from "axios";

// Define Schema of validation with Yup
const loginSchema = Yup.object().shape({
    email: Yup.string().email('Invalid Email format').required('Email is required'),
    password: Yup.string().required('Password is required')
});

// Login Component
// Inital credential for formik
const LoginForm = () => {
    const initialCredentials = {
        email: '',
        password: ''
    }

    let navigate = useNavigate();

    return(
        <div>
            <h4>Login Form</h4>
            {/* Formik to encapsulate a form */}
            <Formik
                initialValues={ initialCredentials }
                validationSchema = { loginSchema}
                onSubmit = { async(values) => {
                    // await new Promise((response) => setTimeout(response, 2000));
                    login(values.email, values.password).then(async (response: AxiosResponse) => {
                        if(response.status === 200){
                            if(response.data.token){  // TODO rgx JWT token
                                // alert(JSON.stringify(response.data, null, 2)); // response.data --> request content
                                // console.table(response.data); // TODO create this method outside      
                                await sessionStorage.setItem('sessionJWTToken', response.data.token)
                                navigate('/');
                            }else{
                                throw new Error('Error generating Login Token')
                            }
                        }else{
                            throw new Error('Invalid Credentials')
                        }
                    }).catch((error) => console.error(`[LOGIN ERROR] Something went wrong: ${error}`))
                }}

                // TODO add a finally after the catch
            >
                {
                    ({values, touched, errors, isSubmitting, handleChange, handleBlur}) =>
                    (
                        <Form>
                            {/* Email field */}
                            <label htmlFor='email' >Email</label>
                            <Field id='email' type='email' name='email' placeholder='example@email.com' />
                            {/* Email Errors */}
                            {
                                errors.email && touched.email && (
                                    <ErrorMessage name="email" component='div'></ErrorMessage>
                                )
                            }

                            {/* Password field */}
                            <label htmlFor='password' >Password</label>
                            <Field id='password' type='password' name='password' placeholder='example' />
                            {/* Password Errors */}
                            {
                                errors.password && touched.password && (
                                    <ErrorMessage name='password' component='div'></ErrorMessage>
                                )
                            }

                            {/* Submit form */}
                            <button type='submit'>
                                Login
                            </button>

                            {/* Message if the form is submitting */}
                            {
                                isSubmitting ? (<p>Checking credentials...</p>) 
                                : null
                            }

                        </Form>
                    )
                }

            </Formik>
        </div>
    )
}

export default LoginForm;


